# NodeJS Command Line Notes

## Getting Started
```shell
$ git clone https://gitlab.com/clarkd34/notes-cli.git
$ npm install
```

## Commands
`add`: Add a new note
- `--title`, `-t`: The title of the note being added [__required__]
- `--body`, `-b`: The content of the note being added [__required__]

`list`: List all saved notes

`read`: Read a specific note
- `--title`, `-t`: The title of the note to fetch [__required__]

`remove`: Remove a saved note
- `--title`, `-t`: The title of the note to remove [__required__]

## Example Usage
```shell
$ node app.js add -t "My Note" -b "This is my note"
Note 'My Note' created

$ node app.js list

Printing 1 note(s):
---
Title: My Note
Body: This is my note
```