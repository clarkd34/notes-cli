const fs = require('fs');

const fetchNotes = () => {
    try {
        const notesString = fs.readFileSync('notes-data.json');
        return JSON.parse(notesString);
    } catch(e) {
        return [];
    }
};

const saveNotes = (notes) => {
    fs.writeFileSync('notes-data.json', JSON.stringify(notes));
};

const addNote = (title, body) => {
    let notes = fetchNotes();
    const note = {
        title,
        body
    };
    const duplicateNotes = notes.filter((note) => title === note.title);
    
    if(duplicateNotes.length === 0) {
        notes.push(note);
        saveNotes(notes);
        return note;
    }
};

const getAll = () => fetchNotes();

const getNote = (title) => {
    const notes = fetchNotes();
    const filteredNotes = notes.filter((note) => title === note.title);
    return filteredNotes[0];
};

const logNote = (note) => {
    debugger;
    console.log('---');
    console.log('Title:', note.title);
    console.log('Body: ', note.body);
};

const removeNote = (title) => {
    const notes = fetchNotes();
    const filteredNotes = notes.filter((note) => title !== note.title);
    saveNotes(filteredNotes);

    return notes.length !== filteredNotes.length;
};

module.exports = {
  addNote,
  getAll,
  getNote,
  logNote,
  removeNote
};