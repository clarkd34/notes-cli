const fs = require('fs');
const _ = require('lodash');
const yargs = require('yargs');
const notes = require('./notes');

const titleOptions = {
    describe: 'Title of note',
    demand: true,
    alias: 't'
};
const bodyOptions = {
    describe: 'Body of note',
    demand: true,
    alias: 'b'
};
const argv = yargs
    .command('add', 'Add a new note', {
        title: titleOptions,
        body: bodyOptions
    })
    .command('list', 'List all available notes')
    .command('read', 'Read a note', {
        title: titleOptions
    })
    .command('remove', 'Remove a note', {
        title: titleOptions
    })
    .help()
    .argv;
const command = process.argv[2];

switch(command) {
    case 'add':
        const newNote = notes.addNote(argv.title, argv.body);
        if(newNote) {
            console.log(`Note '${argv.title}' created`);
        } else {
            console.log(`A note with title '${argv.title}' already exists`);
        }
        break;
    case 'list':
        const allNotes = notes.getAll();
        console.log(`Printing ${allNotes.length} note(s)`)
        allNotes.forEach(notes.logNote);
        break;
    case 'remove':
        var title = notes.removeNote(argv.title);
        const msg = (title) ? 'Note removed' : 'Note not found';
        console.log(msg);
        break;
    case 'read':
        const note = notes.getNote(argv.title);
        if(note) {
            console.log('Note found:');
            notes.logNote(note);
        }
        else {
            console.log('Note not found');
        }
        break;
    default:
        console.log('Command not recognised');
        break;
}
